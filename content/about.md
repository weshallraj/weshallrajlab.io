---
title: "About"
--- 

Hi there. I’m Vishal Raj and this is my personal website.

I'm a graduate from the Birla Institute of Technology and Science.

A Software Engineer with interests in Distributed Systems, Computer Networks, SRE & cloud computing.

Currently, Java, Golang and Javascript are my programming languages of choice (in that particular order).

I'm always fascinated by the abstraction layer just below the one I work on.

I also geek about skateboards, history and choreography.

Currently Reading:
- Undoing Project
- Omnivores Dilemma
- प्रतिनिधि कहानियां

Hit me up on [twitter](https://www.twitter.com/weshallraj/) if you'd like to connect with me!